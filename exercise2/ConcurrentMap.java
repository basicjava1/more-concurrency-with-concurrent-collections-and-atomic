package exercise2;

import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;

public class ConcurrentMap {
    public static void main(String[] args) {
        Map<Character, LongAdder> occurances = new Hashtable<>();

//        ConcurrentMap<Character, LongAdder> occurances = new ConcurrentHashMap<>();
        String str = "ABCD EFGH ABCD";
        for(char character : str.toCharArray()){

//            occurances.computeIfAbsent(character, ch -> new LongAdder()).increment();

            LongAdder longAdder = occurances.get(character);
            if(longAdder == null){
                longAdder = new LongAdder();
            }
            longAdder.increment();
            occurances.put(character, longAdder);
        }
        System.out.println(occurances);
    }
}
