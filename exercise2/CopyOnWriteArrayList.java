package exercise2;

import java.util.List;

public class CopyOnWriteArrayList {
    public static void main(String[] args) {
        List<String> list = new java.util.concurrent.CopyOnWriteArrayList<>();

        list.add("Ant");
        list.add("Bat");
        list.add("Cat");

        for(String element:list)
            System.out.println(element);
    }
}
