package exercise1;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BiCounterWithAtomicInteger {

    // AtomicInteger provides basic Atomic Operations
    // We can remove the lock
    // Complexity is removed from Lock and is given to the AtomicCLass
    private AtomicInteger i = new AtomicInteger();
    private AtomicInteger j = new AtomicInteger();


    public void incrementI(){
        i.incrementAndGet();
    }
    public int getI(){
        return i.get();
    }

    public void incrementJ(){
        j.incrementAndGet();
    }
    public int getJ(){
        return j.get();
    }
}
